# Requirements

main screen logged out
 - user must click to login screen
 - user must click to signup screen
 - user must click image to report screen
 - user must see gallery of demo images
 - user must see date over each image
 - user must see report result over each image
 - user won't sort images by individual
 - user should see developer contacts

main screen logged in
 - user must click to profile screen
 - user must click image to report screen
 - user must upload new image
 - user should see instruction for taking pictures
 - user must see gallery of images
 - user must see date over each image
 - user must see report result over each image
 - user should see developer contacts

login screen
 - user must go back to main screen
 - user must enter email
 - user must enter password
 - user must click to login
 - user must see warning if no account

signup screen
 - user must go back to main screen
 - user must enter email
 - user must enter password
 - user must enter profession
 
profile screen
 - user must see email
 - user must see profession

report screen
 - user must go back to main screen
 - user must see image
 - user must see coloured bookmarks
 - user must see legend for bookmark colours
 - user must click bookmark to hide contour
 - user must click bookmark to show contour
 - user must see tooltip for countour on the bookmark
 - user must download pdf report
 - user must see circle cross
 - user must see crosshairs
